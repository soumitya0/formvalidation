import Header from "./components/Header/Header";
import "./App.css";
import { BrowserRouter } from "react-router-dom";
import Router from "./routes/Routes";
function App() {
  return (
    <BrowserRouter>
      <div style={{ height: "100vh", width: "100vw", background: "#eee" }}>
        <Header />
        {Router}
      </div>
    </BrowserRouter>
  );
}

export default App;
