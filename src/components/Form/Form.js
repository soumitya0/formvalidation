import React, { useState, useRef, useEffect } from "react";

function Form() {
  const [formData, setFormData] = useState({
    email: false,
    fname: false,
    lname: false,
  });

  const refSubmitBtn = useRef(null);

  const errMsg = {
    email: true,
    fname: true,
    lname: true,
  };

  const isValidate = (e) => {
    let val = e.target.value;
    switch (e.target.name) {
      case "fname": {
        if (val.length == 0) {
          setFormData({ ...formData, fname: false });
        } else {
          setFormData({ ...formData, fname: val });
        }

        break;
      }
      case "lname": {
        if (val.length == 0) {
          setFormData({ ...formData, lname: false });
        } else {
          setFormData({ ...formData, lname: val });
        }

        break;
      }
      case "email": {
        if (val.length == 0) {
          setFormData({ ...formData, email: false });
        } else {
          setFormData({ ...formData, email: val });
        }
        break;
      }
    }
  };

  useEffect(() => {
    let values = Object.values(formData);
    if (values.includes(false)) {
      refSubmitBtn.current.setAttribute("disabled", true);
      refSubmitBtn.current.classList.add("gray");
    } else {
      refSubmitBtn.current.removeAttribute("disabled", true);

      refSubmitBtn.current.classList.remove("gray");
      refSubmitBtn.current.classList.add("waves-effect");
    }
  });

  const getValidate = () => {
    if (formData.fname != null) {
      let alphabetPattern = /^[a-zA-Z]*$/;
      let data = formData.fname.trim();
      let result = alphabetPattern.test(data);
      if (result == false) {
        errMsg.fname = "This is invalid please enter alphabet";
      }
    }

    if (formData.lname != null) {
      let alphabetPattern = /^[a-zA-Z]*$/;
      let data = formData.lname.trim();
      let result = alphabetPattern.test(data);
      if (result == false) {
        errMsg.lname = "This is invalid please enter alphabet";
      }
    }

    if (formData.email != null) {
      let emailPattern = /\S+@\S+\.\S+/;
      let data = formData.email.trim();
      let result = emailPattern.test(data);
      if (result == false) {
        errMsg.email = "This is invalid email ";
      }
    }
  };
  const showErrorMsg = () => {
    if (errMsg.fname != true) {
      document.getElementById("errFName").innerHTML = errMsg.fname;
    }
    if (errMsg.lname != true) {
      document.getElementById("errLName").innerHTML = errMsg.lname;
    }
    if (errMsg.email != true) {
      document.getElementById("errEmail").innerHTML = errMsg.email;
    }
  };

  const formSubmit = (e) => {
    e.preventDefault();
    let myFormData = formData;
    console.log(myFormData, "ad");
    getValidate();

    showErrorMsg();

    setTimeout(() => {
      document.getElementById("errFName").innerHTML = "";
      document.getElementById("errLName").innerHTML = "";
      document.getElementById("errEmail").innerHTML = "";
    }, 3000);
  };

  return (
    <div className="container">
      <h4 className="center">Sign up</h4>

      <form id="signup" onSubmit={formSubmit}>
        <label>First Name</label>
        <input type="text" id="fname" name="fname" onChange={isValidate} />
        <p id="errFName" style={{ color: "red", marginTop: "-20px" }}></p>

        <label>Last Name</label>
        <input type="text" id="lname" name="lname" onChange={isValidate} />
        <p id="errLName" style={{ color: "red", marginTop: "-20px" }}></p>

        <label>Email</label>
        <input type="text" id="email" name="email" onChange={isValidate} />
        <p id="errEmail" style={{ color: "red", marginTop: "-20px" }}></p>

        <input
          type="submit"
          className="gray btn"
          disabled
          ref={refSubmitBtn}
          value="submit"
        />
      </form>

      <style jsx>{`
        .element-Disable {
          display: none;
        }
        .element-enable {
          display: block;
        }
        #signup > label {
          font-size: 1.2rem;
          margin-bottom: 25px;
        }

        #signup > input[type="text"],
        #signup > input[type="email"] {
          color: black;
          border: 1px solid #9e9e9e;
          border-radius: 4px;
          outline: none;
          margin-bottom: 35px;
          margin-top: 15px;
        }
      `}</style>
    </div>
  );
}

export default Form;
