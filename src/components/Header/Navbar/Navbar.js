import React, { useEffect } from "react";
import M from "materialize-css/dist/js/materialize.min.js";
import NavbarLinks from "./NavbarLinks";

function Navbar() {
  useEffect(() => {
    let sidenav = document.querySelector("#slide-out");
    M.Sidenav.init(sidenav, {});
  });
  return (
    <>
      <nav className="blue">
        <a className="brand-logo right">Logo</a>

        <a data-target="slide-out" className="sidenav-trigger left">
          <i className="material-icons">menu</i>
        </a>
        <ul id="nav-mobile" className=" hide-on-med-and-down left">
          <li>
            <NavbarLinks title="home" path="/" />
          </li>
          <li>
            <NavbarLinks title="login" path="login" />
          </li>
        </ul>
      </nav>

      <ul id="slide-out" className="sidenav left">
        <li>
          <a href="#item1">Item 1</a>
        </li>
        <li>
          <a href="#item2">Item 2</a>
        </li>
        <li>
          <a href="#item3">Item 3</a>
        </li>
      </ul>
    </>
  );
}

export default Navbar;
