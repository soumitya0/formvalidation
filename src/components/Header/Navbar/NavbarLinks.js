import React from "react";
import { NavLink } from "react-router-dom";

function NavbarLinks({ path, title }) {
  return (
    <div style={{ marginLeft: "31px" }}>
      <NavLink to={title == "home" ? `${path}` : `/${path}`}>{title}</NavLink>
    </div>
  );
}
export default NavbarLinks;
