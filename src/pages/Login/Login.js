import React from "react";
import Form from "../../components/Form/Form";

function Login() {
  return (
    <div style={{ height: "auto", width: "100%" }}>
      <Form />
    </div>
  );
}

export default Login;
