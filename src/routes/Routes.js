import { Route, Switch } from "react-router";
import Home from "../pages/Home/Home";
import Login from "../pages/Login/Login";

const Router = (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route path="/login" component={Login} />
  </Switch>
);

export default Router;
